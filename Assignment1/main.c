/*
* INN365 Assignment 1
* By Alexander Crichton, n6878296
* 09/09/14 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/time.h>

#include "main.h"
#include "producer.h"
#include "consumer.h"

struct Airport airport;
pthread_mutex_t airport_mutex;
pthread_mutex_t exit_mutex;
sem_t land_sem;
sem_t take_off_sem;
int exit_flag;

int main(int argc, char *argv[])
{

	if (check_arguments_valid(argc, argv) == -1)
	{
		return -1;
	}
	int land_chance = atoi(argv[1]);
	int take_off_chance = atoi(argv[2]);

	printf("Welcome to the airport simulator.\n");
	printf("Press p or P followed by return to display the state of the airport.\n");
	printf("Press q or Q followed by return to terminate the simulation.\n\n");
	printf("Press return to start the simulation.\n");
	getchar();

	airport.land_chance = land_chance;
	airport.take_off_chance = take_off_chance;
	for (int i = 0; i < NUM_BAYS; i++)
	{
		airport.bays[i].id = i;
		airport.bays[i].plane[0] = 0;
	}
	
	pthread_mutex_init(&airport_mutex, NULL);
	sem_init(&land_sem, 0, NUM_BAYS);
	sem_init(&take_off_sem, 0, 0);
	pthread_mutex_init(&exit_mutex, NULL);
	srand(time(0));

	pthread_t producer_id;
	pthread_attr_t producer_attr;
	pthread_attr_init(&producer_attr);
	pthread_create(&producer_id, &producer_attr, producer, NULL);

	pthread_t consumer_id;
	pthread_attr_t consumer_attr;
	pthread_attr_init(&consumer_attr);
	pthread_create(&consumer_id, &consumer_attr, consumer, NULL);

	pthread_t reader_id;
	pthread_attr_t reader_attr;
	pthread_attr_init(&reader_attr);
	pthread_create(&reader_id, &reader_attr, reader, NULL);

	pthread_join(producer_id, NULL);
	pthread_join(consumer_id, NULL);
	pthread_join(reader_id, NULL);

	print_airport();

	return 0;
}

int check_arguments_valid(int argc, char *argv[])
{
	if (argc != 3) 
	{
		printf("Useage: <program name> <probability of landing thread producing a plane> <probability of randomly selected landed plane taking off>\n");
		return -1;
	}

	int land_chance = atoi(argv[1]);
	int take_off_chance = atoi(argv[2]);
	if (land_chance < 1 || land_chance > 90 || take_off_chance < 1 || take_off_chance > 90)
	{
		printf("Probability arguments must be between 1 and 90\n");
		return -1;
	}

	return 1;
}

void *reader(void *param)
{
	while (1)
	{
		char input = getchar();

		if (input == 'q' || input == 'Q')
		{
			flag_exit_simulation();
			return 0;
		}
		else if (input == 'p' || input == 'P')
		{
			print_airport();
		}
	}
}

void flag_exit_simulation()
{
	pthread_mutex_lock(&exit_mutex);
	exit_flag = 1;
	pthread_mutex_unlock(&exit_mutex);
	printf("Exiting Simulation.\n");
}

void check_exit_flag()
{
	pthread_mutex_lock(&exit_mutex);
	int exit_value = exit_flag;
	pthread_mutex_unlock(&exit_mutex);
	if (exit_value == 1)
	{
		pthread_exit(0);
	}
}

void print_airport()
{
	pthread_mutex_lock(&airport_mutex);
	printf("\nAirport state:\n");
	for (int i = 0; i < NUM_BAYS; i++)
	{
		if (is_bay_empty(airport.bays[i]))
		{
			printf("%i: Empty\n", i);
		}
		else
		{						
			double duration = get_stay_duration(airport.bays[i].land_time);
			printf("%i: %s (has parked for %.2f seconds)\n", i, airport.bays[i].plane, duration);
		}
	}
	pthread_mutex_unlock(&airport_mutex);
}

int are_all_bays_empty(struct LandingBay bays[])
{
	int ret_val = 1;
	pthread_mutex_lock(&airport_mutex);
	for (int i = 0; i < NUM_BAYS; i++)
	{
		if (!is_bay_empty(bays[i]))
		{
			ret_val = 0;
			break;
		}
	}
	pthread_mutex_unlock(&airport_mutex);
	return ret_val;
}

int are_all_bays_full(struct LandingBay bays[])
{
	int ret_val = 1;
	pthread_mutex_lock(&airport_mutex);
	for (int i = 0; i < NUM_BAYS; i++)
	{
		if (is_bay_empty(bays[i]))
		{
			ret_val = 0;
			break;
		}
	}
	pthread_mutex_unlock(&airport_mutex);
	return ret_val;
}

int get_random_empty_bay(struct LandingBay bays[])
{
	return get_random_bay_by_filter(bays, 1);
}

int get_random_occupied_bay(struct LandingBay bays[])
{
	return get_random_bay_by_filter(bays, 0);
}

int get_random_bay_by_filter(struct LandingBay bays[], int select_empty)
{
	int selected_count = 0;
	struct LandingBay selected_bays[NUM_BAYS];

	for (int i = 0; i < NUM_BAYS; i++)
	{
		if ((select_empty == 1 && is_bay_empty(bays[i]))
			|| (select_empty == 0 && !is_bay_empty(bays[i])))
		{
			selected_bays[selected_count] = bays[i];
			selected_count++;
		}
	}
	if (selected_count > 0)
	{
		int random_index = rand() % selected_count;
		for (int i = 0; i < NUM_BAYS; i++)
		{
			if (bays[i].id == selected_bays[random_index].id)
			{
				return i;
			}
		}

	}

	return -1;
}

int is_bay_empty(struct LandingBay bay)
{
	return bay.plane[0] == 0;
}

double get_stay_duration(struct timeval land_time)
{
	struct timeval now;
	gettimeofday(&now, NULL);
	return diff_in_seconds(land_time, now);
}

double diff_in_seconds(struct timeval x , struct timeval y)
{


    double x_ms , y_ms , diff;
     
    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;
     
    diff = (double)y_ms - (double)x_ms;
     
    return diff / 1.0e6;
}

