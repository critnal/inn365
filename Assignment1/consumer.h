#ifndef CONSUMER_H
#define CONSUMER_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

void *consumer(void *param);
void take_off_plane(struct LandingBay bays[], int take_off_chance);

#endif