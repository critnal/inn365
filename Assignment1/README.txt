To compile the airport simulation:
	- Use the 'make' terminal command in the directory containing the airport simulation source e.g. ~/Airport$ make

To run the airport simulation:
	- Enter './Airport xx xx' in a terminal in the application directory
	- The two xx arguments are the probabilities of the landing thread generating a new plane and the taking of thread removing a plane respectively. These two values must be integers between 10 and 90 inclusive. These two values are required or the application will terminate immediately.