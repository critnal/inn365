#ifndef MAIN_H
#define MAIN_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#define NUM_BAYS 10
#define PLANE_ID_LENGTH 6

struct LandingBay
{
	int id;
	char plane[PLANE_ID_LENGTH];
	struct timeval land_time;
};

struct Airport
{
	struct LandingBay bays[NUM_BAYS];
	int land_chance;
	int take_off_chance;
};

extern struct Airport airport;
extern pthread_mutex_t airport_mutex;
extern pthread_mutex_t exit_mutex;
extern sem_t land_sem;
extern sem_t take_off_sem;
extern int exit_flag;

int check_arguments_valid(int argc, char *argv[]);
void *consumer(void *param);
void *reader(void *param);
void flag_exit_simulation();
void check_exit_flag();
void print_airport();
int are_all_bays_empty(struct LandingBay bays[]);
int are_all_bays_full(struct LandingBay bays[]);
int get_random_empty_bay(struct LandingBay bays[]);
int get_random_occupied_bay(struct LandingBay bays[]);
int get_random_bay_by_filter(struct LandingBay bays[], int select_empty);
int is_bay_empty(struct LandingBay bay);
double get_stay_duration(struct timeval land_time);
double diff_in_seconds(struct timeval x , struct timeval y);

#endif