#ifndef PRODUCER_H
#define PRODUCER_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

void *producer(void *param);
void land_plane(struct LandingBay bays[], int land_chance);
void give_plane_random_id(char *plane);
void put_plane_in_bay(char *plane, int bay_index);

#endif