#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "main.h"
#include "producer.h"

void *producer(void *param)
{
	pthread_mutex_lock(&airport_mutex);
	int land_chance = airport.land_chance;
	struct LandingBay *bays = airport.bays;	
	pthread_mutex_unlock(&airport_mutex);

	int full = 0;
	while (1)
	{

		if (!full && are_all_bays_full(bays))
		{
			printf("The airport is full.\n");
			full = 1;
		}
		else if (full && !are_all_bays_full(bays))
		{
			full = 0;
		}

		land_plane(bays, land_chance);

		check_exit_flag();

		nanosleep((struct timespec[]){{0, 500000000}}, NULL);
	}
}

void land_plane(struct LandingBay bays[], int land_chance)
{
	sem_wait(&land_sem);
	pthread_mutex_lock(&airport_mutex);

	if (land_chance > rand() % 100) 
	{
		int bay_index = get_random_empty_bay(bays);
		if (bay_index >= 0)
		{

			char plane[PLANE_ID_LENGTH];
			give_plane_random_id(plane);
			printf("\nPlane %s is landing ...\n", plane);
			sleep(2);
			strcpy(bays[bay_index].plane, plane);
			gettimeofday(&bays[bay_index].land_time, NULL);
			printf("Plane %s parked in landing bay %d.\n", plane, bay_index + 1);
		}
	}

	pthread_mutex_unlock(&airport_mutex);	
	sem_post(&take_off_sem);
}

void give_plane_random_id(char *plane)
{
	for (int i = 0; i < 2; i++)
	{
		plane[i] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[rand() % 26];
	}
	for (int i = 2; i < 6; i++)
	{
		plane[i] = "0123456789"[rand() % 10];
	}
}