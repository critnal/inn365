#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "main.h"
#include "consumer.h"

void *consumer(void *param)
{
	pthread_mutex_lock(&airport_mutex);
	int take_off_chance = airport.take_off_chance;
	struct LandingBay *bays = airport.bays;	
	pthread_mutex_unlock(&airport_mutex);

	int empty = 0;

	while (1)
	{
		if (!empty && are_all_bays_empty(bays))
		{
			printf("The airport is empty.\n");
			empty = 1;
		}
		else if (empty && !are_all_bays_empty(bays))
		{
			empty = 0;
		}

		take_off_plane(bays, take_off_chance);

		check_exit_flag();

		nanosleep((struct timespec[]){{0, 500000000}}, NULL);
	}
}

void take_off_plane(struct LandingBay bays[], int take_off_chance)
{
	sem_wait(&take_off_sem);
	pthread_mutex_lock(&airport_mutex);

	if (take_off_chance > rand() % 100) 
	{
		int bay_index = get_random_occupied_bay(bays);
		if (bay_index >= 0)
		{
			printf("\nAfter staying at bay %d for %.2f seconds, plane %s is taking off ...\n", 
				bay_index + 1, get_stay_duration(bays[bay_index].land_time), bays[bay_index].plane);
			sleep(2);
			printf("Plane %s has finished taking off.\n", bays[bay_index].plane);
			bays[bay_index].plane[0] = 0;
		}
	}

	pthread_mutex_unlock(&airport_mutex);	
	sem_post(&land_sem);
}

