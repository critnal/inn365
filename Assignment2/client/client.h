#ifndef CLIENT_H
#define CLIENT_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#define DEFAULT_PORT 12345
#define MAX_DATA_SIZE 1000

int validate_arguments(int argc, char **argv, char *host_name, int *specified_port);
void process_input(int server_fd);
void remove_trailing_spaces(char *input);
void send_message(int server_fd, char *message);
void receive_message(int server_fd);
void add_item();

#endif