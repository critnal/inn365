/*
* INN365 Assignment 1
* By Alexander Crichton, n6878296
* 21/10/14 
*/

#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <ctype.h>
#include <netdb.h>

#include "client.h"

int main(int argc, char *argv[])
{
	char host_name[50];
	int specified_port;
	if (validate_arguments(argc, argv, host_name, &specified_port) == -1)
	{
		return -1;
	}

	int server_fd;
	struct hostent *host_ent;
	struct sockaddr_in server_address;

	if ((host_ent = gethostbyname(host_name)) == NULL)
	{
		printf("Error getting host on port %d", specified_port);
		exit(1);
	}
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("Error making socket\n");
		exit(1);
	}

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(specified_port);
	server_address.sin_addr = *((struct in_addr *)host_ent->h_addr_list[0]);
	// bzero(&(server_address.sin_zero), 8);

	if (connect(server_fd, (struct sockaddr *)&server_address, sizeof(struct sockaddr)) == -1)
	{
		perror("Error connecting to socket");
		exit(1);
	}

	printf("Connected\n");

	
	// buffer[num_bytes] = '\0';
	// printf("%s\n", buffer);

	process_input(server_fd);

	close(server_fd);

	return 0;
}

int validate_arguments(int argc, char **argv, char *host_name, int *specified_port) 
{
	if (argc == 3)
	{
		strcpy(host_name, argv[1]);
		*specified_port = atoi(argv[2]);
		if (*specified_port < 81 || *specified_port > 65535)
		{
			printf("Ports must be between 81 and 65535.\n");
			return -1;
		}
	}
	else
	{
		printf("Please enter two arguments, one for the server address and one for the port.\n");
		return -1;
	}

	return 0;
}

void process_input(int server_fd)
{
	int is_adding_item = 0;

	while (1)
	{
		if (is_adding_item == 0)
		{
			printf("Enter a food name to search for it.\n");
			printf("Enter 'add' to add a new food.\n");
			printf("Enter 'q' to quit\n\n");
		}
		else
		{
			printf("Enter an item using the following format (including commas):\n");
			printf("<Name>,<Measure>,<Weight(g)>,<kCal>,<Fat(g)>,<Carbo(g),<Protein>\n\n");			
		}

		char input[1000];
		fgets(input, 1000, stdin);
		if (input[0] == 'q' || input[0] == 'Q')
		{
			break;
		}
		else if (input[0] == 'a' && input[1] == 'd' && input[2] == 'd')
		{
			is_adding_item = 1;
		}
		else 
		{
			if (is_adding_item == 1)
			{
				strncpy(&(input[4]), input, 996);
				strncpy(input, "add:", 4);
				is_adding_item = 0;
			}			
			remove_trailing_spaces(input);
			send_message(server_fd, input);
			receive_message(server_fd);
		}
	}
}

void remove_trailing_spaces(char *input)
{
	for (int i = 0; input[i] != '\n'; i++)
	{
		if (input[i] == ' ')
		{
			input[i] = '\n';
			break;
		}
	}
}

void send_message(int server_fd, char *message) 
{
	// printf("%s", message);
	if (server_fd > 0)
	{
		if (send(server_fd, message, strlen(message), 0) == -1)
		{
			printf("Error sending message: %s\n", message);
		}
	}
}

void receive_message(int server_fd)
{	
	int num_bytes_received = 0;
	char buffer[MAX_DATA_SIZE];

	// while (1)
	// {
		if ((num_bytes_received = recv(server_fd, &buffer, MAX_DATA_SIZE, 0)) == -1)
		{
			perror("Error receiving");
			exit(1);
		}
		else if (num_bytes_received == 0)
		{
			printf("Connection closed\n");
			// break;
		}

		buffer[num_bytes_received] = '\0';
		printf("%s\n", buffer);
	// }

}

void add_item()
{

}