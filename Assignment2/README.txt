The client and server applications are in their own folders. Compile them using the 'make' terminal command from within their source directories, e.g. '~/INN365/Assignment2/client$ make'

Execute the server first using './server xxxx' from the server directory where xxxx is an optional port number. If no port is specified the default port 12345 will be used.

Execute up to ten clients using ./client localhost xxxx' from the client directory where xxxx is the same port as used by the server.