#ifndef SERVER_H
#define SERVER_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#define DEFAULT_PORT 12345
#define MAX_PENDING_CONNECTIONS 10
#define MAX_DATA_SIZE 1000


int validate_arguments(int argc, char **argv, int *specified_port);
void accept_connections();
void handle_connection(void *argument);
void send_message(int client_fd, char *message);
void sigint_handler(int signal);

#endif