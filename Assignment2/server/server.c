/*
* INN365 Assignment 1
* By Alexander Crichton, n6878296
* 21/10/14 
*/

#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <signal.h>

#include "server.h"
#include "database.h"
#include "threadpool.h"

struct ThreadPool *threadpool;
int keep_running = 1;

int main(int argc, char *argv[])
{
	signal(SIGINT, sigint_handler);
	initialise_database();
	threadpool = new_threadpool(10);

	int specified_port;
	if (validate_arguments(argc, argv, &specified_port) == -1)
	{
		return -1;
	}

	int sock_fd;
	struct sockaddr_in server_address;

	if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("Error making socket");
		exit(1);
	}

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(specified_port);
	server_address.sin_addr.s_addr = INADDR_ANY;
	if (bind(sock_fd, (struct sockaddr *)&server_address, sizeof(struct sockaddr)) == -1)
	{
		perror("Error binding socket");
		exit(1);
	}

	if (listen(sock_fd, MAX_PENDING_CONNECTIONS) == -1)
	{
		perror("Error listening");
		exit(1);
	}
	printf("Server is listening on port %d\n", specified_port);

	accept_connections(sock_fd);

	return 0;
}

void accept_connections(int sock_fd)
{
	socklen_t sin_size;
	int client_fd;
	struct sockaddr_in client_address;

	while (keep_running)
	{
		sin_size = sizeof(struct sockaddr_in);
		if ((client_fd = accept(sock_fd, (struct sockaddr *)&client_address, &sin_size)) == -1)
		{
			perror("Error accepting connection");
			continue;
		}
		else 
		{
			printf("Handling connection\n");

			int *temp = malloc(sizeof(*temp));
			*temp = client_fd;
			add_task(threadpool, handle_connection, (void *)temp);
		}
	}
}

int validate_arguments(int argc, char **argv, int *specified_port) 
{
	if (argc == 1)
	{
		*specified_port = DEFAULT_PORT;
	}
	else if (argc == 2)
	{
		*specified_port = atoi(argv[1]);
		if (*specified_port < 81 || *specified_port > 65535)
		{
			printf("Ports must be between 81 and 65535.\n");
			return -1;
		}
	}
	else
	{
		printf("Use either one argument to specify the desired port, or no arguments to use the default port (12345).\n");
		return -1;
	}

	return 0;
}

void handle_connection(void *argument)
{
	int client_fd = *(int*)argument;
	char buffer[MAX_DATA_SIZE];
	int num_bytes_received = 0;
	while (keep_running)
	{
		if ((num_bytes_received = recv(client_fd, &buffer, MAX_DATA_SIZE, 0)) == -1)
		{
			perror("Error receiving");
			exit(1);
		}
		else if (num_bytes_received == 0)
		{
			printf("Connection closed\n");
			break;
		}
		buffer[num_bytes_received] = '\0';
		
		char *message = '\0';
		if (strncmp(buffer, "add:", 4) == 0)
		{
			message = insert_item(buffer);
		}
		else
		{
			message = retrieve_item(buffer);	
		}
		send_message(client_fd, message);
		free(message);

	}
	free(argument);

	close(client_fd);
}

void send_message(int client_fd, char *message) 
{
	// printf("%s", message);
	if (client_fd > 0)
	{
		if (send(client_fd, message, strlen(message), 0) == -1)
		{
			printf("Error sending message: %s\n", message);
		}
	}
}

void sigint_handler(int signal)
{
	destroy_threadpool(threadpool);
	keep_running = 0;
}