#ifndef DATABASE_H
#define DATABASE_H

int initialise_database();
char *retrieve_item(char *input);
struct ItemNode *get_matching_item_list(FILE *file, char *input);
int match_whole_words(char *input, char *line);
void parse_item(struct ItemNode *item_node, char *item);
int count_commas_in_string(char *line);
int get_position_of_comma_index(char *line, int index);
char *get_item_list_as_string(struct ItemNode *item_list);
char *get_item_as_string(struct ItemNode *item_node);
char *insert_item(char *item);
int validate_item(char *item);
int insert_line_into_file(char *line, char *file_name);

#endif