/*
* INN365 Assignment 1
* By Alexander Crichton, n6878296
* 21/10/14 
*/

#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <semaphore.h>

#include "threadpool.h"

#define NUM_THREADS 10
#define MAX_TASKS 10

struct ThreadPoolTask
{
	void (*function)(void *);
	void (*arguments);
};

struct ThreadPool
{
	pthread_mutex_t mutex;
	sem_t queue;
	sem_t dequeue;
	int exit_flag;

	int thread_count;
	pthread_t *threads;

	int task_count;
	struct ThreadPoolTask tasks[MAX_TASKS];
	int head;
	int tail;
};

struct ThreadPool *new_threadpool(int thread_count)
{
	if (thread_count < 1)
	{
		perror("Invalid threadpool arguments");
		return NULL;
	}

	struct ThreadPool *threadpool;
	if ((threadpool = malloc(sizeof(*threadpool))) == NULL)
	{
		perror("Error creating thread pool");
		return NULL;
	}

	if (pthread_mutex_init(&(threadpool->mutex), NULL) != 0)
	{
		perror("Error initialising threadpool mutex");
		free_threadpool(threadpool);
		return NULL;
	}	
	sem_init(&threadpool->queue, 0, MAX_TASKS);
	sem_init(&threadpool->dequeue, 0, 0);

	threadpool->task_count = 0;
	threadpool->head = 0;
	threadpool->tail = 0;
	threadpool->exit_flag = 0;

	threadpool->thread_count = NUM_THREADS;
	threadpool->threads = malloc(sizeof(pthread_t) * thread_count);
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	for (int i = 0; i < thread_count; i++)
	{
		if (pthread_create(&(threadpool->threads[i]), &attr, process_tasks, (void *)threadpool) != 0)
		{
			perror("Error creating thread");
			free_threadpool(threadpool);
			return NULL;
		}
	}

	return threadpool;
}

void add_task(struct ThreadPool *threadpool, void (*function)(void *), void *arguments)
{
	// sem_wait(&(threadpool->queue));
	pthread_mutex_lock(&(threadpool->mutex));

	if (threadpool->task_count >= MAX_TASKS)
	{
		perror("Error adding task: queue full");
		pthread_mutex_unlock(&(threadpool->mutex));
		return;
	}

	threadpool->tasks[threadpool->tail].function = function;
	threadpool->tasks[threadpool->tail].arguments = arguments;

	threadpool->tail++;
	if (threadpool->tail >= MAX_TASKS)
	{
		threadpool->tail = 0;
	}
	threadpool->task_count++;

	pthread_mutex_unlock(&(threadpool->mutex));
	// sem_post(&(threadpool->dequeue));
}

void *process_tasks(void *param)
{
	struct ThreadPool *threadpool = (struct ThreadPool *)param;
	int should_exit = 0;
	while (1)
	{
		pthread_mutex_lock(&(threadpool->mutex));
		should_exit = threadpool->exit_flag;
		pthread_mutex_unlock(&(threadpool->mutex));
		if (should_exit)
		{
			printf("Thread exiting.\n");
			break;
		}
		else
		{
			// sem_wait(&(threadpool->dequeue));
			pthread_mutex_lock(&(threadpool->mutex));

			if (threadpool->task_count <= 0)
			{
				// perror("Error taking task: queue empty");
				pthread_mutex_unlock(&(threadpool->mutex));
			}
			else
			{				
				struct ThreadPoolTask task = threadpool->tasks[threadpool->head];
				threadpool->head++;
				if (threadpool->head >= MAX_TASKS)
				{
					threadpool->head = 0;
				}
				threadpool->task_count--;

				pthread_mutex_unlock(&(threadpool->mutex));

				task.function(task.arguments);
			}
			// sem_post(&(threadpool->queue));
		}
	}
	return NULL;
}

void destroy_threadpool(struct ThreadPool *threadpool)
{
	pthread_mutex_lock(&(threadpool->mutex));
	threadpool->exit_flag = 1;
	pthread_mutex_unlock(&(threadpool->mutex));

	for (int i = 0; i < threadpool->thread_count; i++)
	{
		pthread_join(threadpool->threads[i], NULL);
	}

	free_threadpool(threadpool);
}

void free_threadpool(struct ThreadPool *threadpool)
{	
	pthread_mutex_lock(&(threadpool->mutex));
	if (threadpool->threads)
	{
		free(threadpool->threads);
	}
	if (&(threadpool->mutex))
	{		
		pthread_mutex_destroy(&(threadpool->mutex));
	}
	printf("Freeing threadpool.\n");
	free(threadpool);
}