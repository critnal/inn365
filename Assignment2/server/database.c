#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <strings.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <ctype.h>
#include <regex.h>
#include <pthread.h>

#include "database.h"
#include "item.h"

pthread_mutex_t file_mutex;

int initialise_database()
{
	if (pthread_mutex_init(&file_mutex, NULL) != 0)
	{
		perror("Error initialising database mutex");
		return 0;
	}	
	return 1;
}

char *retrieve_item(char *input)
{
	pthread_mutex_lock(&file_mutex);
	FILE *file = fopen("calories.csv", "r");
	if (file == NULL)
	{
		perror("File does not exist.\n");
		return NULL;
	}

	struct ItemNode *item_list = get_matching_item_list(file, input);
	fclose(file);
	pthread_mutex_unlock(&file_mutex);

	char *result = get_item_list_as_string(item_list);	
	free_item_list(item_list);

	return result;
}

struct ItemNode *get_matching_item_list(FILE *file, char *input)
{
	char line[1000];
	struct ItemNode *item_list = new_item_node();
	while (fgets(line, sizeof(line), file) != NULL)
	{
		if (match_whole_words(input, line))
		{
			struct ItemNode *item_node = new_item_node();
			parse_item(item_node, line);
			add_item_to_list(item_list, item_node);
		}
	}
	return item_list;
}

int match_whole_words(char *input, char *line)
{
	int i = 0;
	while (input[i] != '\n' && (toupper(input[i]) == toupper(line[i])))
	{
		i++;
	}		
	return (input[i] == '\n' && (line[i] == ' ' || line[i] == ','));
}

void parse_item(struct ItemNode *item_node, char *line)
{
	int comma_count = count_commas_in_string(line) - 6;

	int start = 0;
	int end = get_position_of_comma_index(line, comma_count++);
	item_node->item->food = malloc(end - start + 1);
	strncpy(item_node->item->food, &(line[start]), (end - start));	
	item_node->item->food[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count++);
	item_node->item->measure = malloc(end - start + 1);
	strncpy(item_node->item->measure, &(line[start]), (end - start));	
	item_node->item->measure[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count++);
	item_node->item->weight = malloc(end - start + 1);
	strncpy(item_node->item->weight, &(line[start]), (end - start));	
	item_node->item->weight[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count++);
	item_node->item->kCal = malloc(end - start + 1);
	strncpy(item_node->item->kCal, &(line[start]), (end - start));	
	item_node->item->kCal[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count++);
	item_node->item->fat = malloc(end - start + 1);
	strncpy(item_node->item->fat, &(line[start]), (end - start));	
	item_node->item->fat[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count++);
	item_node->item->carbo = malloc(end - start + 1);
	strncpy(item_node->item->carbo, &(line[start]), (end - start));	
	item_node->item->carbo[end - start] = '\0';

	start = end + 1;
	end = get_position_of_comma_index(line, comma_count);
	item_node->item->protein = malloc(end - start + 1);
	strncpy(item_node->item->protein, &(line[start]), (end - start));	
	item_node->item->protein[end - start] = '\0';
}

int count_commas_in_string(char *line)
{
	int count = 0;
	int length = strlen(line);
	for (int i = 0; i < length; i++)
	{
		if (line[i] == ',')
		{
			count++;
		}
	}
	return count;
}

int get_position_of_comma_index(char *line, int index)
{
	int i = 0;
	int count = 0;
	int length = strlen(line);
	for (i = 0; i < length; i++)
	{
		if (line[i] == ',')
		{
			count++;
			if (count > index)
			{
				break;
			}
		}
	}
	return i;
}

char *get_item_list_as_string(struct ItemNode *item_list)
{
	int count = count_item_list(item_list);
	char first_line[50];
	sprintf(first_line, "%d food items found.\n", count);
	char *result = calloc(strlen(first_line) + 1, sizeof(* result));
	strncpy(result, first_line, strlen(first_line));

	struct ItemNode *current_node = item_list->next;
	char *item = NULL;
	int result_size = strlen(result);
	while (current_node != NULL)
	{
		item = get_item_as_string(current_node);
		result_size += strlen(item) + 1;
		if ((result = realloc(result, result_size)) == NULL)
		{
			free(result);
			free(item);
			return NULL;
		}
		sprintf(result, "%s%s", result, item);
		free(item);
		current_node = current_node->next;
	}
	return result;
}

char *get_item_as_string(struct ItemNode *item_node)
{
	char message[1000];
	sprintf(message, "Food: %s\nMeasure: %s\nWeight (g): %s\nkCal: %s\nFat (g): %s\nCarbo (g): %s\nProtein: %s\n", 
		item_node->item->food,
		item_node->item->measure,
		item_node->item->weight,
		item_node->item->kCal,
		item_node->item->fat,
		item_node->item->carbo,
		item_node->item->protein);
	char *return_message = calloc(strlen(message) + 1, sizeof(*return_message));
	strncpy(return_message, message, strlen(message));
	return return_message;
}

char *insert_item(char *message)
{
	printf("%s\n", message);
	char *return_message = malloc(100);

	char *item = malloc(strlen(message) - 3);
	strncpy(item, message+4, strlen(message) - 4);
	printf("%s\n", item);

	// if (validate_item(item))
	// {
	// 	printf("Match\n");
	// }
	if (count_commas_in_string(item) >= 6)
	{
		pthread_mutex_lock(&file_mutex);
		insert_line_into_file(item, "calories.csv");
		pthread_mutex_unlock(&file_mutex);
		sprintf(return_message, "Item added successfully.\n");
	}
	else
	{
		sprintf(return_message, "Item format incorrect.\n");		
	}

	free(item);
	return return_message;
}

// I can't figure out why my validation won't work
int validate_item(char *item)
{
	regex_t regex;
	if (regcomp(&regex, "^([A-z ]+,){1,}([A-z 0-9]+,)([0-9]+,){4}[0-9]+$", 0) != 0)
	{
		perror("Could not compile regex.\n");
		return 0;
	}
	int match = regexec(&regex, item, 0, NULL, 0);
	return match == 0;
}

int insert_line_into_file(char *insert_line, char *file_name)
{
	FILE *read_file = fopen(file_name, "r");
	if (read_file == NULL)
	{
		perror("File does not exist.\n");
		return 0;
	}
	FILE *write_file = fopen("temp.csv", "w");
	if (write_file == NULL)
	{
		perror("Error creating temp write file");
		return 0;
	}

	char read_line[1000];
	int is_line_inserted = 0;

	while (fgets(read_line, sizeof(read_line), read_file) != NULL)
	{
		if (is_line_inserted == 0
			&& read_line[0] != '#'
			&& strcasecmp(insert_line, read_line) < 0)
		{
			fprintf(write_file, "%s", insert_line);
			is_line_inserted = 1;
		}		
		fprintf(write_file, "%s", read_line);
	}
	if (is_line_inserted == 0)
	{
		fprintf(write_file, "%s", insert_line);
		is_line_inserted = 1;		
	}

	fclose(read_file);
	fclose(write_file);

	if (is_line_inserted == 1)
	{
		if (unlink(file_name) < 0)
		{
			perror("Error deleting old file");
		}
		if (rename("temp.csv", file_name) < 0)
		{
			perror("Error renaming new file");
		}
	}

	return is_line_inserted;
}