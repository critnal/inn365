#ifndef ITEM_H
#define ITEM_H

struct Item
{
	char *food;
	char *measure;
	char *weight;
	char *kCal;
	char *fat;
	char *carbo;
	char *protein;
};

struct ItemNode
{
	struct Item *item;
	struct ItemNode *next;
};

struct Item *new_item();
struct ItemNode *new_item_node();
void free_item(struct Item *item);
struct ItemNode *add_item_to_list(struct ItemNode *root, struct ItemNode *new_node);
int count_item_list(struct ItemNode *root);
void free_item_list(struct ItemNode *current_node);

#endif