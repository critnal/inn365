#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <semaphore.h>

#include "item.h"

struct Item *new_item()
{
	struct Item *item = malloc(sizeof(*item));
	if (item == NULL)
	{
		return NULL;
	}
	item->food = NULL;
	item->measure = NULL;
	item->weight = NULL;
	item->kCal = NULL;
	item->fat = NULL;
	item->carbo = NULL;
	item->protein = NULL;
	return item;
}

void free_item(struct Item *item)
{
	free(item->food);
	free(item->measure);
	free(item->weight);
	free(item->kCal);
	free(item->fat);
	free(item->carbo);
	free(item->protein);
	free(item);
}

struct ItemNode *new_item_node()
{
	struct ItemNode *node = malloc(sizeof(*node));
	if (node == NULL)
	{
		return NULL;
	}
	node->item = new_item();
	node->next = NULL;
	return node;
}

struct ItemNode *add_item_to_list(struct ItemNode *current_node, struct ItemNode *new_node)
{
	if (current_node == NULL)
	{
		return NULL;
	}
	while (current_node->next != NULL)
	{
		current_node = current_node->next;
	}
	current_node->next = new_node;
	

	return new_node;
}

int count_item_list(struct ItemNode *root)
{
	int i = 0;
	struct ItemNode *current_node = root->next;
	while (current_node != NULL)
	{
		current_node = current_node->next;
		i++;
	}
	return i;
}

void free_item_list(struct ItemNode *root)
{
	struct ItemNode *temp;
	while (root != NULL)
	{
		temp = root;
		root = root->next;
		
		free_item(temp->item);
		free(temp);
	}
}