#ifndef THREADPOOL_H
#define THREADPOOL_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

struct ThreadPool *new_threadpool(int thread_count);
void add_task(struct ThreadPool *threadpool, void (*function)(void *), void *arguments);
struct ThreadPoolTask *take_task(struct ThreadPool *threadpool);
void *process_tasks(void *param);
void destroy_threadpool(struct ThreadPool *threadpool);
void free_threadpool(struct ThreadPool *threadpool);

#endif